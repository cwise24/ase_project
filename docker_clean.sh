#!/bin/bash

#Container Scalar
dock1="php"
dock2="web"
dock3="maria"

#Images Scalar
image1="mariadb:latest"
image2="nginx:latest"
image3="php:7-fpm"

for c in $dock1 $dock2 $dock3
do
  docker rm -f $c
done

echo "Containers removed"

for i in $image1 $image2 $image3
 do 
   docker rmi $i
   echo "Removed $i"
done

rm -fr ./sql/dbdata
echo "dbdata bye bye"

echo "Work completed"