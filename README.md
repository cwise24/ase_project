Demo showing a `docker-compose` application.  This application consists of two container images:
- nginx:latest
- php:7-fpm
- mariadb:latest

# docker-compose.yml
- [services](https://docs.docker.com/compose/compose-file/compose-versioning/)
- [networks](https://docs.docker.com/compose/networking/)
- web
- php
- maria

# Dockerfile 
PHP comes with php 7.3 and fpm but is missing the necessary mysqli component. Under the `php` directory you will find the dockerfile referenced in the `docker-compolse.yml` .

# web.conf
Using this file to tell nginx how to handle `.php` files.  Pay attention to line 13 as this is the root directory on the php container.

# init.sql
In order to automate this build, the `sql.init` is used to create:
- user & password
- database
- table

# docker_clean.sh
Shell file to clean environment. You will have to also delete the base php image.

# Steps to run
`docker-compose up`

Once maria is up and ready, navigate to http://localhost:8001